#define IS_SWITCH_PRESSEDa0() !(PINB & (1<<PB4))
#define IS_SWITCH_PRESSEDa1() !(PINB & (1<<PB5))
#define IS_SWITCH_PRESSEDa2() !(PINC & (1<<PC2))
#define IS_SWITCH_PRESSEDa3() !(PINC & (1<<PC3))

#define IS_SWITCH_PRESSEDb0() !(PINB & (1<<PB0))
#define IS_SWITCH_PRESSEDb1() !(PINB & (1<<PB1))
#define IS_SWITCH_PRESSEDb2() !(PINB & (1<<PB2))
#define IS_SWITCH_PRESSEDb3() !(PINB & (1<<PB3))

#define ON     1
#define OFF    0

void init_peripheral();
void set_led(uint8_t,uint8_t);
void set_led_value(uint8_t);
uint16_t read_adc(uint8_t);
