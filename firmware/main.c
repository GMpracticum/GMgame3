#include <avr/io.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */
#include <avr/pgmspace.h>   /* required by usbdrv.h */

#include "peri.h"
#include "usbdrv.h"

#define RQ_GET_SWITCHa0 1
#define RQ_GET_SWITCHa1 2
#define RQ_GET_SWITCHa2 3
#define RQ_GET_SWITCHa3 4
#define RQ_GET_SWITCHb0 5
#define RQ_GET_SWITCHb1 6
#define RQ_GET_SWITCHb2 7
#define RQ_GET_SWITCHb3 8
#define RQ_GET_ROTATIONa 9
#define RQ_GET_ROTATIONb 10

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */
usbMsgLen_t usbFunctionSetup(uint8_t data[8])
{
    usbRequest_t *rq = (void *)data;
    static uint8_t switch_state;

    if (rq->bRequest == RQ_GET_SWITCHa0){
        switch_state = IS_SWITCH_PRESSEDa0();

        /* point usbMsgPtr to the data to be returned to host */
        usbMsgPtr = &switch_state;

        /* return the number of bytes of data to be returned to host */
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHa1){
        switch_state = IS_SWITCH_PRESSEDa1();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHa2){
        switch_state = IS_SWITCH_PRESSEDa2();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHa3){
        switch_state = IS_SWITCH_PRESSEDa3();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHb0){
        switch_state = IS_SWITCH_PRESSEDb0();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHb1){
        switch_state = IS_SWITCH_PRESSEDb1();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHb2){
        switch_state = IS_SWITCH_PRESSEDb2();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_SWITCHb3){
        switch_state = IS_SWITCH_PRESSEDb3();
        usbMsgPtr = &switch_state;
        return 1;
    }
    else if (rq->bRequest == RQ_GET_ROTATIONa){
        static uint16_t rotation;
        rotation = read_adc(PC0);
        usbMsgPtr = &rotation;
        return 2;
    }
    else if (rq->bRequest == RQ_GET_ROTATIONb){
        static uint16_t rotation;
        rotation = read_adc(PC1);
        usbMsgPtr = &rotation;
        return 2;
    }
    /* default for not implemented requests: return no data back to host */
    return 0;
}

/* ------------------------------------------------------------------------- */
int main(void)
{
    init_peripheral();

    usbInit();

    /* enforce re-enumeration, do this while interrupts are disabled! */
    usbDeviceDisconnect();
    _delay_ms(300);
    usbDeviceConnect();

    /* enable global interrupts */
    sei();

    /* main event loop */
    for(;;)
    {
        usbPoll();
    }

    return 0;
}

/* ------------------------------------------------------------------------- */
