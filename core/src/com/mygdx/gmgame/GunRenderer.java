package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class GunRenderer {
	private Texture gunLeftT1 = new Texture("Gun8bitLeftPink.png");
	private Texture gunRightT1 = new Texture("Gun8bitRightPink.png");
	private Texture gunLeftT2 = new Texture("Gun8bitLeftBlue.png");
	private Texture gunRightT2 = new Texture("Gun8bitRightBlue.png");
	private TextureRegion gunLeft1 = new TextureRegion(gunLeftT1);
	private TextureRegion gunRight1 = new TextureRegion(gunRightT1);
	private TextureRegion gunLeft2 = new TextureRegion(gunLeftT2);
	private TextureRegion gunRight2 = new TextureRegion(gunRightT2);
	private World world;
	private SpriteBatch batch;
	
	private Vector2 gun1Position = new Vector2(0, 0);
	private Vector2 gun2Position = new Vector2(0, 0);
	
	public GunRenderer(World world){
		this.world = world;
		this.batch = world.getMyGame().batch;
	}
	
	public void render(float delta){
		renderPlayer1();
		renderPlayer2();
	}
	
	int x1, y1, degree1, x2, y2, degree2;
	private void renderPlayer1(){
		x1 = (int) world.getPlayer1().getPosition().x;
		y1 = (int) world.getPlayer1().getPosition().y;
		degree1 = world.getGun1().getDegree();
		
		switch(world.getPlayer1().getStatus()){
		case Player.LEFT:
			//batch.draw(gunLeft, x1, MyGame.HEIGHT - y1);
			batch.draw(gunLeft1, x1 - 50, MyGame.HEIGHT - y1 + 15, 80/2, 30/2, 80, 30, 1, 1, degree1);
			gun1Position.x = x1;
			gun1Position.y = y1 - 55;
			world.getGun1().setPosition(gun1Position.x, gun1Position.y);
			break;
		case Player.RIGHT:
			//batch.draw(gunRight, x1, MyGame.HEIGHT - y1);
			batch.draw(gunRight1, x1 - 25, MyGame.HEIGHT - y1 + 15, 80/2, 30/2, 80, 30, 1, 1, -degree1);
			gun1Position.x = x1 + 10;
			gun1Position.y = y1 - 55;
			world.getGun1().setPosition(gun1Position.x, gun1Position.y);
			break;
		}
	}
	
	private void renderPlayer2(){
		x2 = (int) world.getPlayer2().getPosition().x;
		y2 = (int) world.getPlayer2().getPosition().y;
		degree2 = world.getGun2().getDegree();
		
		switch(world.getPlayer2().getStatus()){
		case Player.LEFT:
			//batch.draw(gunLeft, x2, MyGame.HEIGHT - y2);
			batch.draw(gunLeft2, x2 - 50, MyGame.HEIGHT - y2 + 15, 80/2, 30/2, 80, 30, 1, 1, degree2);
			gun2Position.x = x2;
			gun2Position.y = y2 - 55;
			world.getGun2().setPosition(gun2Position.x, gun2Position.y);
			break;
		case Player.RIGHT:
			//batch.draw(gunRight, x2, MyGame.HEIGHT - y2);
			batch.draw(gunRight2, x2 - 25, MyGame.HEIGHT - y2 + 15, 80/2, 30/2, 80, 30, 1, 1, -degree2);
			gun2Position.x = x2 + 10;
			gun2Position.y = y2 - 55;
			world.getGun2().setPosition(gun2Position.x, gun2Position.y);
			break;
		}
	}
}
