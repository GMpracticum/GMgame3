package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MapRenderer {
	private Map map;
	private SpriteBatch batch;
	private World world;
	//private Texture mapBlockImg = new Texture("BlueMapBlock2.png");
	private Texture mapBlockImg = new Texture("snowMid1.png");
	private Texture mapBlockImg2 = new Texture("snowCenter1.png");
	//private Texture mapCornerBlockImg = new Texture("YellowMapBlock2.png");
	private Texture mapCornerBlockImg = new Texture("stoneCenter1.png");
	private Texture mapCornerBlockImg2 = new Texture("stoneMid1.png");
	
	public MapRenderer(World world) {
		this.batch = world.getMyGame().batch;
		this.map = world.getMap();
		this.world = world;
	}
	
	int x, y;
	public void render(float delta) {
		for(int r = 0; r < world.map.getHeight(); r++ ) {
			for(int c = 0; c < world.map.getWidth(); c++) {
				x = c * WorldRenderer.BLOCK_SIZE;
				y = MyGame.HEIGHT - ( r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
				if(map.MapData[r].charAt(c) == 'x')
					batch.draw(mapBlockImg, x, y);
				else if(map.MapData[r].charAt(c) == 'k')
					batch.draw(mapBlockImg2, x, y);
				else if (map.MapData[r].charAt(c) == '#')
					batch.draw(mapCornerBlockImg, x, y);
				else if (map.MapData[r].charAt(c) == '@')
					batch.draw(mapCornerBlockImg2, x, y);
			}
		}
	}
}