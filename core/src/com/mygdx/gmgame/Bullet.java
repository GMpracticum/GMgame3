package com.mygdx.gmgame;

import com.badlogic.gdx.math.Vector2;

public class Bullet {
	public static final int PLAYER1 = 1;
	public static final int PLAYER2 = 2;
	protected World world;
	protected Map map;
	protected Vector2 position;
	protected Player1 player1;
	protected Player2 player2;
	public boolean isVisible = true, isNull = false, fired = false;
	
	public float speed = 0;
	protected static float speedCharge = (float) 0.62;
	public static float maxSpeed = 80;
	protected float VxSpeed = 0;
	protected float VySpeed = 0;
	protected double degree = 0;
	protected int direction = Player.RIGHT;
	private float gravity = (float) 1;
	
	protected boolean isPressFire = false;
	
	public Bullet(World world) {
		this.world = world;
		this.player1 = world.getPlayer1();
		this.player2 = world.getPlayer2();
		position = new Vector2(0, 0);
	}
	
	public Vector2 getPosition(){
		return position;
	}
	
	protected void move() {
		if(isVisible) {
			position.y = position.y + VySpeed/2;
			VySpeed += gravity;
			position.x = position.x + VxSpeed;
		}
	}
	
	protected double getInverseDegree(double x){
		double result = x;
		for(int i = 1; i <= 360; i++){
			if(x + i == 360)
				result = i;
		}
		return result;
	}
	
	public void reset(){
		isVisible = false;
		isNull = true;
		fired = false;
	}
	
	protected int getRow() {
	    return ((int)position.y) / WorldRenderer.BLOCK_SIZE; 
	}
	
	protected int getColumn() {
	    return ((int)position.x) / WorldRenderer.BLOCK_SIZE; 
	}
	
	public float getSpeed(){
		return speed;
	}
	
	public int getDirection(){
		return direction;
	}
}