package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.TimeUtils;
import practicum.*;

public class World{
	private MyGame myGame;
	public Map map;
	private Player1 player1;
	private Player2 player2;
	private Gun1 gun1;
	private Gun2 gun2;
	private Bullet1 bullet1;
	private Bullet2 bullet2;
	private StatusBar statusBar;
	public boolean p1Win;
	public boolean p2Win;
	public boolean gameStatus;
	static long startTime = TimeUtils.millis();
	static long runTime = TimeUtils.timeSinceMillis(startTime);
	
	private int initPlayer1PositionX = 80, initPlayer1PositionY = 60;
	private int initPlayer2PositionX = 920, initPlayer2PositionY = 60;
	
	private McuWithPeriBoard peri;
	
	World (MyGame myGame, McuWithPeriBoard peri){
		this.myGame = myGame;
		map = new Map(this);
		player1 = new Player1(initPlayer1PositionX, initPlayer1PositionY, this);
		player2 = new Player2(initPlayer2PositionX, initPlayer2PositionY, this);
		gun1 = new Gun1(this);
		gun2 = new Gun2(this);
		bullet1 = new Bullet1(this);
		bullet2 = new Bullet2(this);
		statusBar = new StatusBar(this);
		this.peri = peri;
	}
	
	public void update(float delta){
		gameStatus = !gameStop();
		if(gameStatus) {
			player1.update();
			player2.update();
			gun1.update();
			gun2.update();
			bullet1.update();
			bullet2.update();
			statusBar.update();
		} else {
			gameOver();
		}
		//runTime = TimeUtils.timeSinceMillis(startTime);
		//System.out.println(runTime);
	}
	private boolean gameStop(){
		if(Player1.life == 0)
			p2Win = true;
		if(Player2.life == 0)
			p1Win = true;
		return Player1.life == 0 || Player2.life == 0 ;
	}
	
	public MyGame getMyGame(){
		return myGame;
	}
	
	public Map getMap(){
		return map;
	}	
	
	public Player1 getPlayer1(){
		return player1;
	}
	
	public Player2 getPlayer2(){
		return player2;
	}
	
	public Gun1 getGun1(){
		return gun1;
	}
	
	public Gun2 getGun2(){
		return gun2;
	}
	
	public Bullet1 getBullet1(){
		return bullet1;
	}
	
	public Bullet2 getBullet2(){
		return bullet2;
	}
	
	public StatusBar getStatusBar(){
		return statusBar;
	}
	
	public McuWithPeriBoard getPeri(){
		return peri;
	}
	private void gameOver() {
		if(Gdx.input.isKeyPressed(Keys.ENTER)) { 
			gameReset();
		}
	}
	private void gameReset() {
		gameStatus = true;
		Player1.life = 3;
		Player2.life = 3;
		p1Win = false;
		p2Win = false;
		map.resetMap();
		player1.position.x = initPlayer1PositionX;
		player1.position.y = initPlayer1PositionY;
		player2.position.x = initPlayer2PositionX;
		player2.position.y = initPlayer2PositionY;
	}
}