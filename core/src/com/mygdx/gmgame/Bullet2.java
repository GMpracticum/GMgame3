package com.mygdx.gmgame;

public class Bullet2 extends Bullet {
	public Bullet2(World world) {
		super(world);
	}
	
	private void fire(){
		/*if(Gdx.input.isKeyPressed(Keys.ENTER)){
			if(!isPressFire){
				speed = 0;
				isPressFire = true;
			} else {
				if(speed < maxSpeed)
					speed += 0.5;
			}
		} else if(!fired) {
			if(isPressFire){
				fired = true;
				world.getStatusBar().charge2 = 0;
				isPressFire = false;
				isVisible = true;
				world.getStatusBar().playerTurn = StatusBar.PLAYER2;
				if(world.getPlayer2().status == Player.RIGHT)
					degree = (float) (getInverseDegree(world.getGun2().getDegree()) + 90);
				else
					degree = (float) (world.getGun2().getDegree() - 90);
				double angleInRadian = Math.toRadians(degree);
				double sin = Math.sin(angleInRadian);
				double cos = Math.cos(angleInRadian);
				
				position.x = world.getGun2().getPosition().x;
				position.y = world.getGun2().getPosition().y;
				VySpeed = (float) (speed * cos);
				VxSpeed = (float) (speed * sin);
			}
		}*/
		
		if(world.getPeri().getSwitchb3()){
			if(!isPressFire){
				speed = 0;
				isPressFire = true;
			} else {
				if(speed < maxSpeed)
					speed += speedCharge;
			}
		} else if(!fired) {
			if(isPressFire){
				fired = true;
				isPressFire = false;
				isVisible = true;
				direction = world.getPlayer2().getStatus();
				world.getStatusBar().playerTurn = StatusBar.PLAYER2;
				if(world.getPlayer2().status == Player.RIGHT)
					degree = (float) (getInverseDegree(world.getGun2().getDegree()) + 90);
				else
					degree = (float) (world.getGun2().getDegree() - 90);
				if(degree > 360)
					degree = -degree - 180;
				double angleInRadian = Math.toRadians(degree);
				double sin = Math.sin(angleInRadian);
				double cos = Math.cos(angleInRadian);
				
				position.x = world.getGun2().getPosition().x;
				position.y = world.getGun2().getPosition().y;
				VySpeed = (float) (speed * cos);
				VxSpeed = (float) (speed * sin);
				speed = 0;
			}
		}
	}
	
	protected void bulletHit() {
		map = world.getMap();
		if(world.map.canBulletHit(getRow(), getColumn(), PLAYER2)) {
			world.map.deleteBlock(getRow(), getColumn());
			reset();
    	} else if(( getRow()==player1.getRow() || getRow()==player1.getRow()-1) && (getColumn()==player1.getColumn() || getColumn()==player1.getColumn()+1 )&& isVisible) {
    		Player1.life--;
    		reset();
    	} else if(( getRow()==player2.getRow() || getRow()==player2.getRow()-1) && (getColumn()==player2.getColumn() || getColumn()==player2.getColumn()+1 ) && isVisible) {
    		Player2.life--;
    		reset();
    	}
	}
	
	public void update(){
		fire();
		move();
		bulletHit();
	}
}
