package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WorldRenderer {
	private World world;
	private SpriteBatch batch;
	private MapRenderer mapRenderer;
	private PlayerRenderer playerRenderer;
	private GunRenderer gunRenderer;
	private BulletRenderer bulletRenderer;
	private StatusBarRenderer statusBarRenderer;
	
	public static final int BLOCK_SIZE = 20;
	private Texture gameOverImg;
	private Texture enterImg;
	private Texture p1WinImg;
	private Texture p2WinImg;
	private Texture background;
	
	public WorldRenderer(World world, MapRenderer mapRenderer, PlayerRenderer playerRenderer, GunRenderer gunRenderer, BulletRenderer bulletRenderer, StatusBarRenderer statusBarRenderer) {
		this.world = world;
		this.batch = world.getMyGame().batch;
		this.mapRenderer = mapRenderer;
		this.playerRenderer = playerRenderer;
		this.gunRenderer = gunRenderer;
		this.bulletRenderer = bulletRenderer;
		this.statusBarRenderer = statusBarRenderer;
		gameOverImg = new Texture("gameover.png");
		enterImg = new Texture("enter.png");
		p1WinImg = new Texture("redwin.png");
		p2WinImg = new Texture("bluewin.png");
		background = new Texture("spaceBackground1.jpg");
	}
	
	public void render(float delta) {
		batch.begin();
		batch.draw(background, 0, 0);
		mapRenderer.render(delta);
		playerRenderer.render(delta);
		gunRenderer.render(delta);
		bulletRenderer.render(delta);
		statusBarRenderer.render(delta);
		gameOver_render();
		batch.end();
	}
	
	public void gameOver_render(){
		if(!world.gameStatus) {
			batch.draw(gameOverImg, 360, 400);
			batch.draw(enterImg,420,200);
			if(world.p1Win) {
				batch.draw(p1WinImg,700,150);
			}
			if(world.p2Win) {
				batch.draw(p2WinImg,700,150);
			}
		}
	}
}