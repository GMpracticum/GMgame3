package com.mygdx.gmgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class Player2 extends Player {
	public static int life = 3;
    public Player2(int x, int y, World world) {
    	position = new Vector2(x, y);
    	this.world = world;
    }
    
    public void update(){
    	if(Gdx.input.isKeyJustPressed(Keys.UP)){
    		onJump();
    	}
    	if(world.getPeri().getSwitchb2()){
    		onJump();
    	}
    	
     	if(Gdx.input.isKeyPressed(Keys.LEFT)){
            status = LEFT;
    		if(canMoveInDirection()) {
    			position.x -= speed;
    		}
    	}
     	if(world.getPeri().getSwitchb0()){
            status = LEFT;
    		if(canMoveInDirection()) {
    			position.x -= speed;
    		}
    	}
     	
    	if(Gdx.input.isKeyPressed(Keys.RIGHT)){
    		status = RIGHT;
    		if(canMoveInDirection()) {
    			position.x += speed;
    		}
    	}
    	if(world.getPeri().getSwitchb1()){
    		status = RIGHT;
    		if(canMoveInDirection()) {
    			position.x += speed;
    		}
    	}
    	onGravity();
    }
}
