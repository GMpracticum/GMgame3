package com.mygdx.gmgame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class PlayerRenderer {
	private static final int PLAYER1 = 11;
	private static final int PLAYER2 = 12;
	private World world;
	private SpriteBatch batch;
	private Texture player1Img = new Texture("char1.png");
	private TextureRegion player1ImgR = new TextureRegion(player1Img);
	private Texture player2Img = new Texture("char2.png");
	private TextureRegion player2ImgR = new TextureRegion(player2Img);
	private Texture heartImg = new Texture("heart.png");
	private Vector2 pos1, pos2;
	
	public PlayerRenderer(World world){
		this.world = world;
		this.batch = world.getMyGame().batch;
	}
	
	public void render(float delta) {
		renderPlayer(PLAYER1);
		renderPlayer(PLAYER2);
	}
	
	private void renderPlayer(int player){
		switch(player){
			case PLAYER1:
				pos1 = world.getPlayer1().getPosition();
				batch.draw(player1Img, pos1.x - 15, MyGame.HEIGHT - pos1.y - 15);
				//batch.draw(player1ImgR, pos1.x, MyGame.HEIGHT - pos1.y - 15, 35/2, 43/2, 35, 43, 1, 1, 0);
				switch(Player1.life){
					case 1 : batch.draw(heartImg, pos1.x, MyGame.HEIGHT - pos1.y + 60);
							 break;
					case 2 : batch.draw(heartImg, pos1.x - 10, MyGame.HEIGHT - pos1.y + 60);	
							 batch.draw(heartImg, pos1.x + 10, MyGame.HEIGHT - pos1.y + 60);
							 break;
					case 3 : batch.draw(heartImg, pos1.x - 20, MyGame.HEIGHT - pos1.y + 60);
							 batch.draw(heartImg, pos1.x , MyGame.HEIGHT - pos1.y + 60);	
							 batch.draw(heartImg, pos1.x + 20, MyGame.HEIGHT - pos1.y + 60);
							 break;
				}
				break;
			case PLAYER2:
				pos2 = world.getPlayer2().getPosition();
				batch.draw(player2Img, pos2.x - 15, MyGame.HEIGHT - pos2.y - 15);
				switch(Player2.life){
					case 1 : batch.draw(heartImg, pos2.x, MyGame.HEIGHT - pos2.y + 60);
							 break;
					case 2 : batch.draw(heartImg, pos2.x - 10, MyGame.HEIGHT - pos2.y + 60);	
							 batch.draw(heartImg, pos2.x + 10, MyGame.HEIGHT - pos2.y + 60);
							 break;
					case 3 : batch.draw(heartImg, pos2.x - 20, MyGame.HEIGHT - pos2.y + 60);
							 batch.draw(heartImg, pos2.x , MyGame.HEIGHT - pos2.y + 60);	
							 batch.draw(heartImg, pos2.x + 20, MyGame.HEIGHT - pos2.y + 60);
							 break;
				}
				break;	
		}
	}
}