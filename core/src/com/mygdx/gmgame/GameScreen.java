package com.mygdx.gmgame;

import org.usb4java.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;

import practicum.*;

public class GameScreen extends ScreenAdapter {
	private World world;
	private WorldRenderer worldRenderer;
	private MapRenderer mapRenderer;
	private PlayerRenderer playerRenderer;
	private GunRenderer gunRenderer;
	private BulletRenderer bulletRenderer;
	private StatusBarRenderer statusBarRenderer;
	
	private McuWithPeriBoard peri;
	
	public GameScreen(MyGame myGame) {
		McuBoard.initUsb();
		Device[] devices = McuBoard.findBoards();
		if (devices.length == 0) {
            System.out.format("** Practicum board not found **\n");
            return;
    	}
    	else {
            System.out.format("** Found %d practicum board(s) **\n", devices.length);
    	}
        peri = new McuWithPeriBoard(devices[0]);
        
		world = new World(myGame, peri);
		mapRenderer = new MapRenderer(world);
		playerRenderer = new PlayerRenderer(world);
		gunRenderer = new GunRenderer(world);
		bulletRenderer = new BulletRenderer(world);
		statusBarRenderer = new StatusBarRenderer(world);
		worldRenderer = new WorldRenderer(world, mapRenderer, playerRenderer, gunRenderer, bulletRenderer, statusBarRenderer);
	}
	
	public void render(float delta) {
    	update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldRenderer.render(delta);
	}
	
	public void update(float delta) {
		world.update(delta);
	}
	
	public McuWithPeriBoard getPeri(){
		return peri;
	}
}