package practicum;
import org.usb4java.Device;
public class McuWithPeriBoard extends McuBoard
{
    private static final byte RQ_GET_SWITCHa0 = 1;
    private static final byte RQ_GET_SWITCHa1 = 2;
    private static final byte RQ_GET_SWITCHa2 = 3;
    private static final byte RQ_GET_SWITCHa3 = 4;
    private static final byte RQ_GET_SWITCHb0 = 5;
    private static final byte RQ_GET_SWITCHb1 = 6;
    private static final byte RQ_GET_SWITCHb2 = 7;
    private static final byte RQ_GET_SWITCHb3 = 8;
    private static final byte RQ_GET_ROTATIONa = 9;
    private static final byte RQ_GET_ROTATIONb = 10;
    public McuWithPeriBoard(Device device) {
		super(device);
	}
    public boolean getSwitcha0(){
        byte[] ret = this.read(RQ_GET_SWITCHa0, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitcha1(){
        byte[] ret = this.read(RQ_GET_SWITCHa1, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitcha2(){
        byte[] ret = this.read(RQ_GET_SWITCHa2, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitcha3(){
        byte[] ret = this.read(RQ_GET_SWITCHa3, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitchb0(){
        byte[] ret = this.read(RQ_GET_SWITCHb0, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitchb1(){
        byte[] ret = this.read(RQ_GET_SWITCHb1, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitchb2(){
        byte[] ret = this.read(RQ_GET_SWITCHb2, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public boolean getSwitchb3(){
        byte[] ret = this.read(RQ_GET_SWITCHb3, (short) 0, (short) 0);
        return ret[0] == 1;
    }
    
    public int getRotationa(){
        byte[] ret = this.read(RQ_GET_ROTATIONa, (short) 0, (short) 0);
        return (ret[0] & 0xFF) + (ret[1] & 0xFF)*256;
    }
    
    public int getRotationb(){
        byte[] ret = this.read(RQ_GET_ROTATIONb, (short) 0, (short) 0);
        return (ret[0] & 0xFF) + (ret[1] & 0xFF)*256;
    }
}